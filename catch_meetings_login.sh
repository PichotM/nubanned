#!/bin/bash

output='"startDate": "01/06/2020",'
counter=0

while echo "$output" | grep -q "\"startDate\": \"01/06/2020\""; do
  output=$(curl -s -i "https://nubanner.neu.edu/StudentRegistrationSsb/ssb/searchResults/getFacultyMeetingTimes?term=202034&courseReferenceNumber=40116")
  counter=$((counter+1))
  printf "\r%d" $counter
done

echo "\n$output"
