FORMAT: 1A
HOST: https://nubanner.neu.edu/StudentRegistrationSsb/ssb

# NU Banner API

This web page documents usage of the public-facing API for browsing classes on
[*NU Banner*](https://nubanner.neu.edu/StudentRegistrationSsb/).
It is not official; all this information was discovered through reverse-engineering.
We are unaffiliated with both Northeastern University and Ellucian.
See the [README on Gitlab](https://gitlab.com/jennydaman/nubanned#nu-banner-api)
for more information.

Listing data returns *JSON*, while most endpoints that describe a specific course
will return *HTML* instead.

# Group StudentRegistrationSsb

## Terms [/classSearch/getTerms{?offset,max,searchTerm}]

### List semesters [GET]

Generates a list of semesters and their corresponding term codes that
are available to be viewed.

+ Parameters

    + offset: `1` (number, required) - Start index (keep it as 1)
    + max: `10` (number, required) - How many semesters to return (change this if you want)
    + searchTerm (string, optional) - Key word to filter the results by

+ Response 200 (application/json)
    
    + Headers
    
            Set-Cookie: JSESSIONID=ABCDEF0123456789ABCDEF0123456789;path=/StudentRegistrationSsb;Secure;HttpOnly
            Set-Cookie: nubanner-cookie=0123456789.12345.1234; path=/; Httponly; Secure

    + Body
    
            [
              {
                "code": "201960",
                "description": "Summer 2 2019 Semester"
              },
              {
                "code": "201954",
                "description": "Summer 2019 CPS Semester"
              },
              {
                "code": "201950",
                "description": "Summer Full 2019 Semester"
              },
              {
                "code": "201940",
                "description": "Summer 1 2019 Semester"
              },
              {
                "code": "201938",
                "description": "Spring 2019 Law Quarter"
              },
              {
                "code": "201935",
                "description": "Spring 2019 CPS Quarter"
              },
              {
                "code": "201934",
                "description": "Spring 2019 CPS Semester"
              },
              {
                "code": "201932",
                "description": "Spring 2019 Law Semester"
              },
              {
                "code": "201930",
                "description": "Spring 2019 Semester"
              },
              {
                "code": "201928",
                "description": "Winter 2019 Law Quarter (View Only)"
              }
            ]

## Subjects [/classSearch/get_subject{?searchTerm,term,offset,max,_}]

### Search for subjects [GET]

*nubanner* includes `&_=1550718949982` in the query string, but the function
is unknown and the value is probably ignored.

+ Parameters

    + term: `201930` (number, required) - The term code for the semester to search in
    + offset: `1` (number, required) - Start index (keep it as 1)
    + max: `10` (number, required) - How many subjects to return (change this if you want)
    + searchTerm: `Bio` (string, optional) - Key word to filter the results by

+ Response 200 (application/json)

            [
              {
                "code": "BIOC",
                "description": "Biochemistry"
              },
              {
                "code": "BIOE",
                "description": "Bioengineering"
              },
              {
                "code": "BINF",
                "description": "Bioinformatics"
              },
              {
                "code": "BIOL",
                "description": "Biology"
              },
              {
                "code": "BIOT",
                "description": "Biotechnology"
              },
              {
                "code": "CHEM",
                "description": "Chemistry &amp; Chemical Biology"
              },
              {
                "code": "EEMB",
                "description": "Ecology, Evolutn &amp; Marine Biol"
              }
            ]

## clickContinue [/term/search]

### Declare term [POST]

Tells *nubanner* which term the client is interested in,
and will grant permission to the client session (cookies `JSESSIONID` and `nubanner-cookie`)
to get [`/searchResults/searchResults`](#studentregistrationssb-search).

If provided cookies, then the current session is authorized.
Otherwise, authorized cookies are returned in the response with `Set-Cookie`.

*NU Banner* uses the query string `?mode=search`,
however it doesn't seem to be necessary.

:::note
If you forget `term` in the body, then you'll get a response containing these values:

```json
"regAllowed": false,
"studentEligFailures": 
["No Classes Found in this Date Range"]
```

There is no validation that the specified term is correct.

:::

+ Request term code (application/x-www-form-urlencoded; charset=UT)

    + Body
    
            term=201930&studyPath=&studyPathText=&startDatepicker=&endDatepicker=

    + Headers

            Cookie: JSESSIONID=ABCDEF0123456789ABCDEF0123456789; nubanner-cookie=0123456789.12345.1234;

+ Response 200 (application/json)

            {
              "fwdURL": "/StudentRegistrationSsb/ssb/classSearch/classSearch"
            }
            

## Search [/searchResults/searchResults{?txt_subject,txt_courseNumber,txt_term,startDatepicker,endDatepicker,pageOffset,pageMaxSize,sortColumn,sortDirection}]

Produces a list of courses that matches the given criteria.

::: warning
#### <i class="fa fa-warning"></i> Warning
Fresh sessions are *blocked* from getting course data.
Before getting the `/searchResults/searchResults` endpoint,
the client must first get permission by
[POSTing the term code to `/term/search`](#studentregistrationssb-clickcontinue-post).

Otherwise, `/searchResults/searchResults` will return `"success": false`.
:::

### Search courses [GET]
+ Parameters

    + txt_subject: `CS` (string, optional) - Subject code
    + txt_courseNumber: `2500` (number, optional) - Course number
    + txt_term: `201930` (number, optional) - term code&mdash;must match what was POSTed to [`/term/search`](#studentregistrationssb-clickcontinue-post)
    + startDatepicker (optional)
    + endDatepicker (optional)
    + pageOffset: `0` (number, optional) - Start index (default 0)
    + pageMaxSize: `10` (number, optional) - How many sessions to return (default 20). **Maximum number of results is 500.**
    + sortColumn: `subjectDescription` (string, optional) - how to sort the results
    + sortDirection: `asc` (string, optional) - direction to sort the columns

+ Request

    + Headers

            Cookie: JSESSIONID=ABCDEF0123456789ABCDEF0123456789; nubanner-cookie=0123456789.12345.1234;

+ Response 200 (application/json)

            {
              "success": true,
              "totalCount": 4,
              "data": [
                {
                  "id": 207227,
                  "term": "201930",
                  "termDesc": "Spring 2019 Semester",
                  "courseReferenceNumber": "30340",
                  "partOfTerm": "1",
                  "courseNumber": "2500",
                  "subject": "CS",
                  "subjectDescription": "Computer Science",
                  "sequenceNumber": "01",
                  "campusDescription": "Boston",
                  "scheduleTypeDescription": "Lecture",
                  "courseTitle": "Fundamentals of Computer Science 1",
                  "creditHours": null,
                  "maximumEnrollment": 75,
                  "enrollment": 71,
                  "seatsAvailable": 4,
                  "waitCapacity": 0,
                  "waitCount": 0,
                  "waitAvailable": 0,
                  "crossList": null,
                  "crossListCapacity": null,
                  "crossListCount": null,
                  "crossListAvailable": null,
                  "creditHourHigh": null,
                  "creditHourLow": 4,
                  "creditHourIndicator": null,
                  "openSection": true,
                  "linkIdentifier": null,
                  "isSectionLinked": false,
                  "subjectCourse": "CS2500",
                  "faculty": [
                    {
                      "bannerId": "000550992",
                      "category": null,
                      "class": "net.hedtech.banner.student.faculty.FacultyResultDecorator",
                      "courseReferenceNumber": "30340",
                      "displayName": "Mislove, Alan",
                      "emailAddress": null,
                      "primaryIndicator": true,
                      "term": "201930"
                    }
                  ],
                  "meetingsFaculty": [
                    {
                      "category": "01",
                      "class": "net.hedtech.banner.student.schedule.SectionSessionDecorator",
                      "courseReferenceNumber": "30340",
                      "faculty": [],
                      "meetingTime": {
                        "beginTime": "0915",
                        "building": "EV",
                        "buildingDescription": "East Village",
                        "campus": "BOS",
                        "campusDescription": "Boston",
                        "category": "01",
                        "class": "net.hedtech.banner.general.overall.MeetingTimeDecorator",
                        "courseReferenceNumber": "30340",
                        "creditHourSession": 4,
                        "endDate": "04/17/2019",
                        "endTime": "1020",
                        "friday": false,
                        "hoursWeek": 3.25,
                        "meetingScheduleType": "LEC",
                        "meetingType": "CLAS",
                        "meetingTypeDescription": "Class",
                        "monday": true,
                        "room": "024",
                        "saturday": false,
                        "startDate": "01/07/2019",
                        "sunday": false,
                        "term": "201930",
                        "thursday": true,
                        "tuesday": false,
                        "wednesday": true
                      },
                      "term": "201930"
                    }
                  ],
                  "reservedSeatSummary": null,
                  "sectionAttributes": [
                    {
                      "class": "net.hedtech.banner.student.schedule.SectionDegreeProgramAttributeDecorator",
                      "code": "NCFQ",
                      "courseReferenceNumber": "30340",
                      "description": " NUpath Formal/Quant Reasoning",
                      "isZTCAttribute": false,
                      "termCode": "201930"
                    },
                    {
                      "class": "net.hedtech.banner.student.schedule.SectionDegreeProgramAttributeDecorator",
                      "code": "NCND",
                      "courseReferenceNumber": "30340",
                      "description": " NUpath Natural/Designed World",
                      "isZTCAttribute": false,
                      "termCode": "201930"
                    },
                    {
                      "class": "net.hedtech.banner.student.schedule.SectionDegreeProgramAttributeDecorator",
                      "code": "NCT1",
                      "courseReferenceNumber": "30340",
                      "description": " NU Core Science/Tech Lvl 1",
                      "isZTCAttribute": false,
                      "termCode": "201930"
                    },
                    {
                      "class": "net.hedtech.banner.student.schedule.SectionDegreeProgramAttributeDecorator",
                      "code": "UBCS",
                      "courseReferenceNumber": "30340",
                      "description": "Computer&amp;Info Sci",
                      "isZTCAttribute": false,
                      "termCode": "201930"
                    }
                  ]
                },
                {... other results omitted ...}
              ],
              "pageOffset": 0,
              "pageMaxSize": 10,
              "sectionsFetchedCount": 4,
              "pathMode": "search",
              "searchResultsConfigs": {...},
              "ztcEncodedImage": ...
            }

## Reset Form [/classSearch/resetDataForm]

Resets the input from last search, so that you can search for a different subject and course number.
The server remembers your inquiry.
If you try to search for a different class using the same session, it will not work.

### Reset data form [POST]

+ Request

    + Headers

            Cookie: JSESSIONID=ABCDEF0123456789ABCDEF0123456789; nubanner-cookie=0123456789.12345.1234;

+ Response 200 (text/html;charset=utf-8)

            true

## Contact Card [/contactCard/retrieveData{?bannerId,termCode}]

This endpoint isn't very useful, since the contact info (email and phone) are always `null`.

### Staff Information [GET]

+ Parameters

    + termCode: `201930` (number, required) - The term code for the semester
    + bannerId: `000933475` (number, required) - From [`searchResults`](#studentregistrationssb-search-get)

+ Response 200 (application/json)

        {
          "data": {
            "facultyCardPopupConfig": 
            [
              {
                "config": "facultyCardPopup",
                "display": "Faculty Card Popup",
                "title": "Faculty Card Popup"
              },
              {
                "config": "facultyName",
                "display": "Faculty Name",
                "title": "Faculty Name"
              },
              {
                "config": "facultyPhoto",
                "display": "Faculty Photo",
                "title": "Faculty Photo"
              },
              {
                "config": "facultyTitle",
                "display": "Faculty Title",
                "title": "Faculty Title"
              },
              {
                "config": "facultyDepartment",
                "display": "Faculty Department",
                "title": "Faculty Department"
              },
              {
                "config": "facultyEmail",
                "display": "Faculty Email Address",
                "title": "Faculty Email Address"
              }
            ],
            "personData": {
              "lastName": "Shivers III",
              "firstName": "Olin",
              "middleName": null,
              "fullName": "Olin Shivers III",
              "displayName": "Shivers III, Olin",
              "preferenceFirstName": "Olin",
              "namePrefix": "Dr",
              "nameSuffix": null,
              "scheduleIndicator": "Y",
              "advisorIndicator": "Y",
              "title": {
                "class": "net.hedtech.banner.student.system.FacultyCategory",
                "id": 1,
                "version": 0,
                "id": 1,
                "code": "PROF",
                "dataOrigin": null,
                "description": "Professor",
                "lastModified": "10/29/2008",
                "lastModifiedBy": null
              },
              "deptAndCollegeInformation": null,
              "address": null,
              "telephone": null,
              "email": null
            },
            "message": ""
          }
        }



# Group searchResults

Most (but one) of these are `POST` endpoints that require the same URL encoded body,
and returns HTML code.

## Class Details [/searchResults/getClassDetails]

### Class Details [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

        <span class="status-bold">Associated Term: </span>Spring 2019 Semester <br/>
        <span class="status-bold">CRN: </span><span id="courseReferenceNumber">30392</span><br/>
        
        <span class="status-bold">Campus: </span>Boston  <br/>
        <span class="status-bold">Schedule Type: </span>
        
            Lecture
        
        <br/>
        <span class="status-bold">Instructional Method: </span>Traditional<br/>
        <span class="status-bold">Section Number: </span><span id="sectionNumber">02</span>  <br/>
        <span class="status-bold">Subject: </span><span id="subject">Physics</span>  <br/>
        <span class="status-bold">Course Number: </span><span id="courseNumber">1147</span>  <br/>
        <span class="status-bold">Title: </span><span id="courseTitle">Physics for Life Sciences 2</span><br/>
        
        <span class="status-bold">Credit Hours:</span>
        
            4
            <br>
        
        <span class="status-bold">Grade Mode: </span>
            No Section specified grade mode, please see Catalog link below for more information.<br>

## Bookstore [/searchResults/getSectionBookstoreDetails]

### Bookstore Links [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

        <span class="status-bold">Bookstore Links</span><br/><br/>
        
        <span>Use the link(s) below to access online bookstores to determine the cost of required materials.</span><br/><br/>
        
        <div class="indent-left">
            
                    <a href="https://www.barnesandnoble.com" target="_blank">Barnes and Noble</a> <br/>
                <br/>
            
                    <a href="http://www.bkstr.com/webapp/wcs/stores/servlet/CourseMaterialsResultsView?storeId=&lt;&lt;Campus Store ID&gt;&gt;&amp;termId=201930&amp;departmentDisplayName=PHYS&amp;courseDisplayName=Physics&amp;sectionDisplayName=1147" target="_blank">Follett Bookstore</a> <br/>
                <br/>
            
                    <a href="http://northeastern.bncollege.com" target="_blank">bookstore.links.NUBookstore</a> <br/>
                <br/>
            
        </div>

## Course Description [/searchResults/getCourseDescription]

### Course Description [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

        Continues PHYS 1145. Covers heat, electricity, vibrations and waves, sound, geometrical optics, and nuclear physics and radioactivity. The application of physics to a variety of problems in the life and health sciences is emphasized. Electricity topics include electrostatics, capacitance, resistivity, direct-current circuits, and RC circuits. Vibrations and waves topics include simple harmonic motion and wave motion. Sound topics include wave characteristics, the ear, Doppler effect, shock waves, and ultrasound. Optics topics include reflection, mirrors, refraction, total internal reflection, fiber optics, lenses, the eye, telescopes, and microscopes. Nuclear physics and radioactivity topics include atomic nucleus, radioactivity, half-life, radioactive dating, detectors, nuclear reaction, fission, fusion, radiation damage, radiation therapy, PET, and MRI. A laboratory is included. 

## Syllabus [/searchResults/getSyllabus]

Unfortunately, the syllabus is uploaded for only a few courses.

### Syllabus [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

            No Syllabus Information Available

## Section Attributes [/searchResults/getSectionAttributes]

Information about NUPath.

### Attributes [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

             <div>
                <span class='attribute-text'> NUpath Natural/Designed World  NCND
                    <br/></span></div>
            
             <div>
                <span class='attribute-text'>UG College of Science  UBSC
                    </span></div>

## Restrictions [/searchResults/getRestrictions]

### Restrictions [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

            <div class="infoicon"><span class="status-bold">Not all restrictions are applicable.</span></div> <br/>
            
                
                    <span class="status-bold">Must be enrolled in one of the following Levels: </span><br/>
                        
                            <span class="detail-popup-indentation">Undergraduate (UG)</span><br/>
                        
                
                <br/>

## Meeting Times [/searchResults/getFacultyMeetingTimes{?term,courseReferenceNumber}]


::: warning
#### <i class="fa fa-warning"></i> Warning
Rarely, maybe once out of 5,000 requests, you'll get an HTTP 302 redirect
to the Shibboleth SSO login page <small title="https://neuidmsso.neu.edu/idp/profile/cas/login?TARGET=https%3A%2F%2Fnubanner.neu.edu%2FStudentRegistrationSsb%2Fj_spring_cas_security_check">(neuidmsso.neu.edu)</small> instead.
Here is a [script](https://gitlab.com/jennydaman/nubanned/blob/master/catch_meetings_login.sh)
to test this inconsistency using cURL.
:::

### Instructor/Meeting Times [GET]

+ Parameters

    + term: `201930` (number, required) - The term code for the semester
    + courseReferenceNumber: `30393` (number, required) - From [`searchResults`](#studentregistrationssb-search-get)

+ Response 200 (application/json)

        {
          "fmt": 
          [
            {
              "category": "01",
              "class": "net.hedtech.banner.student.schedule.SectionSessionDecorator",
              "courseReferenceNumber": "30393",
              "faculty": 
              [
                {
                  "bannerId": "000124859",
                  "category": "01",
                  "class": "net.hedtech.banner.student.faculty.FacultyResultDecorator",
                  "courseReferenceNumber": "30393",
                  "displayName": "Kravchenko, Sergey",
                  "emailAddress": null,
                  "primaryIndicator": true,
                  "term": "201930"
                },
                {
                  "bannerId": "001676905",
                  "category": "01",
                  "class": "net.hedtech.banner.student.faculty.FacultyResultDecorator",
                  "courseReferenceNumber": "30393",
                  "displayName": "Fotovatian, Assad",
                  "emailAddress": null,
                  "primaryIndicator": false,
                  "term": "201930"
                }
              ],
              "meetingTime": {
                "beginTime": "1335",
                "building": "CG",
                "buildingDescription": "Cargill Hall",
                "campus": "BOS",
                "campusDescription": "Boston",
                "category": "01",
                "class": "net.hedtech.banner.general.overall.MeetingTimeDecorator",
                "courseReferenceNumber": "30393",
                "creditHourSession": 4.0,
                "endDate": "04/17/2019",
                "endTime": "1440",
                "friday": false,
                "hoursWeek": 3.25,
                "meetingScheduleType": "LEC",
                "meetingType": "CLAS",
                "meetingTypeDescription": "Class",
                "monday": true,
                "room": "097",
                "saturday": false,
                "startDate": "01/07/2019",
                "sunday": false,
                "term": "201930",
                "thursday": true,
                "tuesday": false,
                "wednesday": true
              },
              "term": "201930"
            },
            {
              "category": "01",
              "class": "net.hedtech.banner.student.schedule.SectionSessionDecorator",
              "courseReferenceNumber": "30393",
              "faculty": 
              [
                {
                  "bannerId": "000124859",
                  "category": "01",
                  "class": "net.hedtech.banner.student.faculty.FacultyResultDecorator",
                  "courseReferenceNumber": "30393",
                  "displayName": "Kravchenko, Sergey",
                  "emailAddress": null,
                  "primaryIndicator": true,
                  "term": "201930"
                },
                {
                  "bannerId": "001676905",
                  "category": "01",
                  "class": "net.hedtech.banner.student.faculty.FacultyResultDecorator",
                  "courseReferenceNumber": "30393",
                  "displayName": "Fotovatian, Assad",
                  "emailAddress": null,
                  "primaryIndicator": false,
                  "term": "201930"
                }
              ],
              "meetingTime": {
                "beginTime": "1030",
                "building": "EL",
                "buildingDescription": "Ell Hall",
                "campus": "BOS",
                "campusDescription": "Boston",
                "category": "01",
                "class": "net.hedtech.banner.general.overall.MeetingTimeDecorator",
                "courseReferenceNumber": "30393",
                "creditHourSession": 0.0,
                "endDate": "04/23/2019",
                "endTime": "1230",
                "friday": false,
                "hoursWeek": 2.0,
                "meetingScheduleType": "LEC",
                "meetingType": "FNEX",
                "meetingTypeDescription": "Final Exam",
                "monday": false,
                "room": "AUD",
                "saturday": false,
                "startDate": "04/23/2019",
                "sunday": false,
                "term": "201930",
                "thursday": false,
                "tuesday": true,
                "wednesday": false
              },
              "term": "201930"
            }
          ]
        }

## Enrollment Info [/searchResults/getEnrollmentInfo]

### Enrollment/Waitlist [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

        <span class="status-bold">Enrollment Actual:</span> <span dir="ltr"> 94 </span><br/>
        <span class="status-bold">Enrollment Maximum:</span> <span dir="ltr"> 155 </span><br/>
        <span class="status-bold">Enrollment Seats Available:</span> <span dir="ltr"> 61 </span>  <br/>
        <hr/>
        <span class="status-bold">Waitlist Capacity:</span> <span dir="ltr"> 0  </span><br/>
        <span class="status-bold">Waitlist Actual:</span> <span dir="ltr"> 0  </span><br/>
        <span class="status-bold">Waitlist Seats Available:</span> <span dir="ltr"> 0  </span><br/>

## Corequisites [/searchResults/getCorequisites]

::: warning
#### <i class="fa fa-warning"></i> Warning
The structure of the returned HTML table is inconsistent!
Different classes might return either 3 or 5 columns.
:::

<a href="different_corequisite_tables.png" style="display: block; max-width: 100%">
    <img src="different_corequisite_tables.png" alt="screenshot" style="width: 100%">
</a>


### Corequisites [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

                <table class="basePreqTable">
                    <thead>
                    <tr>
                        <th>Subject</th>
                        <th>Course Number</th>
                        <th>Title</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                        <tr>
                            <td>Physics</td>
                            <td>1148</td>
                            <td>Lab for PHYS 1147</td>
                        </tr>
                    
                    </tbody>
                </table>

+ Request (application/x-www-form-urlencoded)

        term=202010&courseReferenceNumber=18505

* Response 200 (text/html)
    
        <table>
            <thead>
                <tr>
                    <th>CRN</th>
                    <th>Subject</th>
                    <th>Course Number</th>
                    <th>Title</th>
                    <th>Section</th>
                </tr>
            </thead>
            <tbody>
                
                    <tr>
                        <td>18504</td>
                        <td>Health Sci - Interdisciplinary</td>
                        <td>1200</td>
                        <td>Basic Skills for the Healthcare Professional</td>
                        <td>02</td>
                    </tr>
                
            </tbody>
        </table>


## Prerequisites [/searchResults/getSectionPrerequisites]

### Prerequisites [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

        <h3>Catalog Prerequisites</h3>
        
          
            <table class="basePreqTable">
              <thead>
              <tr>
                <th>And/Or</th>
                <th></th>
                <th>Test</th>
                <th>Score</th>
                <th>Subject</th>
                <th>Course Number</th>
                <th>Level</th>
                <th>Grade</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              
                <tr>
                  
                    <td></td>
                  
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Physics</td>
                  <td>1145</td>
                  <td>Undergraduate</td>
                  <td>D-</td>
                  <td></td>
                </tr>
              
                <tr>
                  
                    <td>Or</td>
                  
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Physics</td>
                  <td>1149</td>
                  <td>Undergraduate</td>
                  <td>D-</td>
                  <td></td>
                </tr>
              
                <tr>
                  
                    <td>Or</td>
                  
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Physics</td>
                  <td>1151</td>
                  <td>Undergraduate</td>
                  <td>D-</td>
                  <td></td>
                </tr>
              
                <tr>
                  
                    <td>Or</td>
                  
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Physics</td>
                  <td>1161</td>
                  <td>Undergraduate</td>
                  <td>D-</td>
                  <td></td>
                </tr>
              
                <tr>
                  
                    <td>Or</td>
                  
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Physics</td>
                  <td>1171</td>
                  <td>Undergraduate</td>
                  <td>D-</td>
                  <td></td>
                </tr>
              
              </tbody>
            </table>

+ Request (application/x-www-form-urlencoded)

        term=201730&courseReferenceNumber=35984

+ Response 200 (text/html)

        <h3>Catalog Prerequisites</h3>
        
          
            <table class="basePreqTable">
              <thead>
              <tr>
                <th>And/Or</th>
                <th></th>
                <th>Test</th>
                <th>Score</th>
                <th>Subject</th>
                <th>Course Number</th>
                <th>Level</th>
                <th>Grade</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              
                <tr>
                  
                    <td></td>
                  
                  <td>(</td>
                  <td></td>
                  <td></td>
                  <td>Mathematics</td>
                  <td>2321</td>
                  <td>Undergraduate</td>
                  <td>C-</td>
                  <td></td>
                </tr>
              
                <tr>
                  
                    <td>And</td>
                  
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Physics</td>
                  <td>1155</td>
                  <td>Undergraduate</td>
                  <td>C-</td>
                  <td></td>
                </tr>
              
                <tr>
                  
                    <td>And</td>
                  
                  <td>(</td>
                  <td></td>
                  <td></td>
                  <td>Chemistry &amp; Chemical Biology</td>
                  <td>1151</td>
                  <td>Undergraduate</td>
                  <td>C-</td>
                  <td></td>
                </tr>
              
                <tr>
                  
                    <td>Or</td>
                  
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Chemistry &amp; Chemical Biology</td>
                  <td>1211</td>
                  <td>Undergraduate</td>
                  <td>C-</td>
                  <td>)</td>
                </tr>
              
                <tr>
                  
                    <td></td>
                  
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>)</td>
                </tr>
              
                <tr>
                  
                    <td>Or</td>
                  
                  <td></td>
                  <td>Graduate Admission</td>
                  <td>REQ</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              
              </tbody>
            </table>

## Mutual Exclusion [/searchResults/getCourseMutuallyExclusions]

### Mutual Exclusion [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

            No Mutual Exclusion information available.

## Cross Listed Courses [/searchResults/getXlstSections]

### Cross Listed Courses [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

            No cross list information available.

## Linked Sections [/searchResults/getLinkedSections]

### Linked Sections [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

            No linked course information available.

## Fees [/searchResults/getFees]

### Fees [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

            No fee information available.

## Catalog [/searchResults/getSectionCatalogDetails]

Summary of section information, including title, college, department, hours,
levels, grading mode, type of class (lecture or lab), and NUPath attributes.

### Catalog [POST]

+ Request (application/x-www-form-urlencoded)

        term=201930&courseReferenceNumber=30392

+ Response 200 (text/html)

        <span class="status-bold">Title: </span>Physics for Life Sciences 2<br/>
        
        <span class="status-bold">College:</span>
        
                <span>College of Science  SC</span>
            
        <br/>
        
        <span class="status-bold">Division:</span>
        
        <br/>
        
        <span class="status-bold">Department:</span>
        
        <span>Physics  PHYS</span>
            
        <br/>
        
        <span class="status-bold">Hours:</span><br/>
        <span class="indent-left">Credit Hours: </span><span class="credit-hours-direction">4  </span><br/>
        
            <div class="indent-left">
                Lecture: <span class="credit-hours-direction">4  </span><br/>
                Lab: <span class="credit-hours-direction">  </span> <br/>
                Other: <span class="credit-hours-direction">  </span> <br/>
            </div>
        
        
        <span class="status-bold">Levels:</span><br/>
        <div class="indent-left">
            
                Undergraduate  UG
            
        </div>
        <span class="status-bold">Grading Modes:</span>
        <div class="indent-left">
            Pass/Fail  P<br/> Traditional  S 
        </div>
        <span class="status-bold">Schedule Types:</span>
        <div class="indent-left">Lecture  LEC </div>
        <span class="status-bold">Attributes:</span>
        <div class="indent-left"> NUpath Natural/Designed World  NCND<br/> UG College of Science  UBSC </div>

