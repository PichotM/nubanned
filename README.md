# NU Banner API

Unofficial developer documentation for NU Banner's public-facing RESTful JSON API.

## Introduction

Ellucian Banner software is widely used by many colleges and universities.
It manages the database for student and class information.
Version 9 (codename "*Banner EX*") provides a front-end web application
powered by jQuery and AJAX (XMLHttpRequest).

NU Banner is an instance of *Banner EX* for course registration at **Northeastern University**.
https://nubanner.neu.edu/StudentRegistrationSsb/ssb/registration

## Motivation

Having access to course schedule data, students can develop creative third-party
applications that add convenience to everyone's lives: students, professors, and advisers.

Despite that the data itself is public, the official developer's guide
(called the "Ellucian XE Registry") is proprietary.

https://github.com/ryanhugh/searchneu/issues/57

Usage of the JSON API was discovered through reverse-engineering. By providing this information,
I hope it will be easier for student developers to contribute to their educational institutions.

## Notes

### Unimportant Parameters

These cookies on the domain `*.neu.edu` are irrelevant:
`IDMSESSID`, `_gq`, `_gat_Ellucian`, and `_gid`.

These HTTP request headers below can be safely disregarded:
`X-Synchronizer-Token`, `Host`, `Referer`, and `User-Agent`.

### Comments on Security

Banner EX's front end is thousands of lines of jQuery, littered with unsafe HTML templates and
JavaScript `eval`... Personally, I wouldn't be surprised if there are a couple of injection vulnerabilities.

On [NU Banner](https://nubanner.neu.edu/StudentRegistrationSsb/ssb/registration), there is a hard-coded
JS object in the head that specifies `"admin": false`. That makes me nervous. Also, it tries to retrieve a
non-existent stylesheet from `http://themeserver:8080`.
## Links

- https://searchneu.com
- [Banner XE Stack 2016](https://www.aits.uillinois.edu/UserFiles/Servers/Server_474/File/Ellucian%20Live/Navigating%20the%20Banner%20XE%20Tech%20Stack.pdf)


